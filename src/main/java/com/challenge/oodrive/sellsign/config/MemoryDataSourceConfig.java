package com.challenge.oodrive.sellsign.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "memoryEntityManagerFactory",
        transactionManagerRef = "memoryTransactionManager",
        basePackages = {"com.challenge.oodrive.sellsign.repository.memory"})
public class MemoryDataSourceConfig {

	
    @Primary
    @Bean(name = "memoryDataSourceProperties")
    @ConfigurationProperties("app.oodrive.datasource.dbmemory")
    public DataSourceProperties memoryDataSourceProperties() {
        return new DataSourceProperties();
    }
    
    @Primary
    @Bean(name = "memoryDataSource")
    @ConfigurationProperties("app.oodrive.datasource.dbmemory.configuration")
    public DataSource memoryDataSource(@Qualifier("memoryDataSourceProperties") DataSourceProperties memoryDataSourceProperties) {
        return memoryDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }
    
    @Primary
    @Bean(name = "memoryEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean memoryEntityManagerFactory(
            EntityManagerFactoryBuilder memoryEntityManagerFactoryBuilder, @Qualifier("memoryDataSource") DataSource memoryDataSource) {

        Map<String, String> secondaryJpaProperties = new HashMap<>();
        secondaryJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        secondaryJpaProperties.put("hibernate.hbm2ddl.auto", "create-drop");

        return memoryEntityManagerFactoryBuilder
                .dataSource(memoryDataSource)
                .packages("com.challenge.oodrive.sellsign.model.memory")
                .persistenceUnit("memoryDataSource")
                .properties(secondaryJpaProperties)
                .build();
    }
    
    @Primary
    @Bean(name = "memoryTransactionManager")
    public PlatformTransactionManager memoryTransactionManager(
            @Qualifier("memoryEntityManagerFactory") EntityManagerFactory memoryEntityManagerFactory) {

        return new JpaTransactionManager(memoryEntityManagerFactory);
    }
    
}
