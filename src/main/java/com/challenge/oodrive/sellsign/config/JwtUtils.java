package com.challenge.oodrive.sellsign.config;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.challenge.oodrive.sellsign.service.UserDetailsInfo;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtUtils {

	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${app.oodrive.jwtSecret}")
	private String jwtSercret;

	@Value("${app.oodrive.jwtExpirationTime}")
	private int jwtExpirationTime;

	public String getUserNameFromJwt(String token) {
		return Jwts.parser().setSigningKey(jwtSercret).parseClaimsJws(token).getBody().getSubject();
	}

	public String generateJwtToken(Authentication authentication) {

		UserDetailsInfo userPrincipal = (UserDetailsInfo) authentication.getPrincipal();

		return Jwts.builder()
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationTime))
				.signWith(SignatureAlgorithm.HS512, jwtSercret)
				.compact();
	}
	
	public boolean validateJwtToken(String token) {

		try {
			Jwts.parser().setSigningKey(jwtSercret).parseClaimsJws(token);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature : {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token : {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("Invalid JWT is expired : {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("Invalid JWT is unsupported : {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims is empty : {}", e.getMessage());
		}
		return false;
	}

}