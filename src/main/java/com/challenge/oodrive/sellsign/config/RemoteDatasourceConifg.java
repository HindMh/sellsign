package com.challenge.oodrive.sellsign.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "remoteEntityManagerFactory",
        transactionManagerRef = "remoteTransactionManager",
        basePackages = {"com.challenge.oodrive.sellsign.repository.remote"})
public class RemoteDatasourceConifg {
	
    @Bean(name = "remoteDataSourceProperties")
    @ConfigurationProperties("app.oodrive.datasource.remote")
    public DataSourceProperties remoteDataSourceProperties() {
        return new DataSourceProperties();
    }
    
    @Bean(name = "remoteDataSource")
    @ConfigurationProperties("app.oodrive.datasource.remote.configuration")
    public DataSource remoteDataSource(@Qualifier("remoteDataSourceProperties") DataSourceProperties remoteDataSourceProperties) {
        return remoteDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }
    
    @Bean(name = "remoteEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean remoteEntityManagerFactory(
            EntityManagerFactoryBuilder remoteEntityManagerFactoryBuilder, @Qualifier("remoteDataSource") DataSource remoteDataSource) {

        Map<String, String> secondaryJpaProperties = new HashMap<>();
        secondaryJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        secondaryJpaProperties.put("hibernate.hbm2ddl.auto", "update");

        return remoteEntityManagerFactoryBuilder
                .dataSource(remoteDataSource)
                .packages("com.challenge.oodrive.sellsign.model.remote")
                .persistenceUnit("remoteDataSource")
                .properties(secondaryJpaProperties)
                .build();
    }
    
    @Bean(name = "remoteTransactionManager")
    public PlatformTransactionManager remoteTransactionManager(
            @Qualifier("remoteEntityManagerFactory") EntityManagerFactory remoteEntityManagerFactory) {

        return new JpaTransactionManager(remoteEntityManagerFactory);
    }

}
