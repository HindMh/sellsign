package com.challenge.oodrive.sellsign.model.memory;

public enum RoleEnum {
	
	ROLE_USER, ROLE_ADMIN;

}
