package com.challenge.oodrive.sellsign.model.remote;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "recipients")
@Data 
@NoArgsConstructor 
@AllArgsConstructor
@ToString
public class Recipients implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer civility;
	
	private String firstname;
	
	private String lastname;
	
	private String email;
	
	@Column(name="address_1")
	private String adress;
	
	@Column(name = "postal_code")
	private String zipCode;
	
	private String city;
	
	@Column( name="cell_phone")
	private String cellPhone;
	
	@Column( name="last_modification_date")
	private Long lastUpdateDate;
	
	@Column( name="last_modification_place")
	private Integer lastUpdatePlace;
	
	private String localisation;
	
}
