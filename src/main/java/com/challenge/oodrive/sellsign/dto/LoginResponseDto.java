package com.challenge.oodrive.sellsign.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponseDto {

	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;
	
	public LoginResponseDto(String token,Long id, String username, String email, List<String> roles) {
		super();
		this.token = token;
		this.id=id;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}
	
	
	
	
}
