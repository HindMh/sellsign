package com.challenge.oodrive.sellsign.dto;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data 
@NoArgsConstructor 
@AllArgsConstructor
@ToString
public class RecipientDto implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private Long id;
	
	private Integer civility;
	
	private String firstname;
	
	private String lastname;
	
	private String email;
	
	
	private String adress;
	
	
	private String zipCode;
	
	private String city;
	
	
	private String cellPhone;
	
	private Long lastUpdateDate;
	
	private Integer lastUpdatePlace;
	
	private String localisation;
	
}
