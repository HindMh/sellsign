package com.challenge.oodrive.sellsign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.challenge.oodrive.sellsign.model.memory.Role;
import com.challenge.oodrive.sellsign.model.memory.RoleEnum;
import com.challenge.oodrive.sellsign.model.memory.User;
import com.challenge.oodrive.sellsign.repository.memory.RoleRepository;
import com.challenge.oodrive.sellsign.repository.memory.UserRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class SellSignApplication implements CommandLineRunner{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder encoder;
	
	public static void main(String[] args) {
		SpringApplication.run(SellSignApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Role r1 = new Role();
		r1.setName(RoleEnum.ROLE_ADMIN);
		
		Role r2 = new Role();
		r2.setName(RoleEnum.ROLE_USER);
		
		roleRepository.save(r1);
		roleRepository.save(r2);
		
		User u = new User();
		u.setUsername("admin");
		u.setPassword(encoder.encode("admin"));
		u.setEmail("admin@yahoo.fr");
		u.getRoles().add(r1);
		userRepository.save(u);
		
	}

}
