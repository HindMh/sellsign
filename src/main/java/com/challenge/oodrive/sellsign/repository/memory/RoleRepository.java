package com.challenge.oodrive.sellsign.repository.memory;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.oodrive.sellsign.model.memory.Role;
import com.challenge.oodrive.sellsign.model.memory.RoleEnum;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	public Optional<Role> findByName(RoleEnum name);

}
