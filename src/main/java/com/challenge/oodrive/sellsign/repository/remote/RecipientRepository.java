package com.challenge.oodrive.sellsign.repository.remote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.oodrive.sellsign.model.remote.Recipients;

@Repository
public interface RecipientRepository extends JpaRepository<Recipients, Long> {

	public Recipients findByEmail(String email);
	
	public boolean existsByEmail(String email);
}
