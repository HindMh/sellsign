package com.challenge.oodrive.sellsign.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.oodrive.sellsign.config.JwtUtils;
import com.challenge.oodrive.sellsign.dto.LoginDto;
import com.challenge.oodrive.sellsign.dto.LoginResponseDto;
import com.challenge.oodrive.sellsign.dto.UserRegistryDto;
import com.challenge.oodrive.sellsign.model.memory.Role;
import com.challenge.oodrive.sellsign.model.memory.RoleEnum;
import com.challenge.oodrive.sellsign.model.memory.User;
import com.challenge.oodrive.sellsign.repository.memory.RoleRepository;
import com.challenge.oodrive.sellsign.repository.memory.UserRepository;
import com.challenge.oodrive.sellsign.service.UserDetailsInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api( description="API pour la gestion des utilisateurs")
@RestController
@RequestMapping("/api/auth")
@CrossOrigin("*")
public class AuthenticationResource {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@ApiOperation(value="L'authentification avec login & pwd")
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDto user) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
			
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsInfo userDetails = (UserDetailsInfo) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new LoginResponseDto(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@ApiOperation(value="Création d'un nouveau utilisateur ")
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserRegistryDto user) {

		if (userRepository.existsByUsername(user.getUsername())) {
			return ResponseEntity.badRequest().body("Error : username already exist");
		}
		if (userRepository.existsByEmail(user.getEmail())) {
			return ResponseEntity.badRequest().body("Error : email already exist");
		}

		User userEntity = new User();
		userEntity.setUsername(user.getUsername());
		userEntity.setEmail(user.getEmail());
		userEntity.setPassword(encoder.encode(user.getPassword()));

		Set<Role> roles = new HashSet<>();
		Role roleFound = roleRepository.findByName(RoleEnum.ROLE_USER)
				.orElseThrow(() -> new RuntimeException("Role not found"));
		roles.add(roleFound);
		userEntity.setRoles(roles);
		userRepository.save(userEntity);
		return ResponseEntity.ok("User registred success");

	}
}
