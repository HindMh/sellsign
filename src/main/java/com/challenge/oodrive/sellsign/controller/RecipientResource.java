package com.challenge.oodrive.sellsign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.oodrive.sellsign.dto.RecipientDto;
import com.challenge.oodrive.sellsign.service.RecepientService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api( description="API pour les opérations CRUD sur les recipients.")
@RestController
@RequestMapping("/api/recipient")
@CrossOrigin("*")
public class RecipientResource {

	@Autowired
	private RecepientService serviceRecepient;
	
	@ApiOperation(value="Récupération de liste des recipients")
	@GetMapping("/list")
	public ResponseEntity<?> getAllRecipient() {
		return ResponseEntity.ok(serviceRecepient.findAllRecipients());
	}

	@ApiOperation(value="Récupération et recherche d'un récipient par mail")
	@GetMapping("/filter")
	public ResponseEntity<?> findReciptientByEmail(@RequestParam(name = "email") String email) {
		return ResponseEntity.ok(serviceRecepient.findOneRecipientByEmail(email));
	}

	@ApiOperation(value="Création de nouveau recipient")
	@PostMapping("/create")
	public ResponseEntity<?> createRecipient(@RequestBody RecipientDto dto) {

		try {
			return ResponseEntity.ok(serviceRecepient.createRecipient(dto));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		
	}

	@ApiOperation(value="Modification d'un recipient existant")
	@PutMapping("/update/{recipientId}")
	public ResponseEntity<?> updateRecipient(@PathVariable Long recipientId, @RequestBody RecipientDto dto) {

		try {
			return ResponseEntity.ok(serviceRecepient.updateRecipient(recipientId, dto));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}

	}

	@ApiOperation(value="Suppression d'un recipient existant")
	@DeleteMapping("/delete/{recipientId}")
	public ResponseEntity<?> deleteRecipient(@PathVariable Long recipientId) {
		serviceRecepient.deleteRecipient(recipientId);
		return ResponseEntity.ok("Delete successful !!");
	}

}
