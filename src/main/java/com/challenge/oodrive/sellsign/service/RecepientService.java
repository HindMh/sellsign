package com.challenge.oodrive.sellsign.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.challenge.oodrive.sellsign.dto.RecipientDto;
import com.challenge.oodrive.sellsign.model.remote.Recipients;
import com.challenge.oodrive.sellsign.repository.remote.RecipientRepository;

@Service
public class RecepientService {

	private final RecipientRepository recipientRepository;
	
	public RecepientService(RecipientRepository recipientRepository) {
		this.recipientRepository = recipientRepository;
	}
	
	@Transactional
	public RecipientDto createRecipient(RecipientDto dto) throws Exception {
		
		if (recipientRepository.existsByEmail(dto.getEmail())) {
			throw new Exception("Error : email already exist");
		}
		
		Recipients entity = this.toEntity(dto);
		Recipients savedRecipient = recipientRepository.save(entity);
		return toDto(savedRecipient);
		
	}
	
	@Transactional
	public RecipientDto updateRecipient(Long recipientId, RecipientDto dto) {
		
		Recipients recipientFound = recipientRepository.findById(recipientId)
					.orElseThrow(() -> new EntityNotFoundException(String.format("Recipient with id %d not found", recipientId)));
		
		recipientFound.setFirstname(dto.getFirstname());
		recipientFound.setLastname(dto.getLastname());
		recipientFound.setCivility(dto.getCivility());
		recipientFound.setAdress(dto.getAdress());
		recipientFound.setCellPhone(dto.getCellPhone());
		recipientFound.setCity(dto.getCity());
		recipientFound.setEmail(dto.getEmail());
		recipientFound.setZipCode(dto.getZipCode());
		recipientFound.setLocalisation(dto.getLocalisation());
		recipientFound.setLastUpdateDate(new Date().getTime());
		recipientFound.setLastUpdatePlace(dto.getLastUpdatePlace());
		recipientRepository.save(recipientFound);
		return toDto(recipientFound);
		
	}
	
	@Transactional
	public void deleteRecipient(Long recipientId) {
		recipientRepository.deleteById(recipientId);
	}
	
	public List<RecipientDto> findAllRecipients() {
		return Optional.ofNullable(recipientRepository.findAll())
				.orElse(Collections.emptyList())
				.stream()
				.map(this::toDto)
				.collect(Collectors.toList());
	}
	
	public List<RecipientDto> findOneRecipientByEmail(String email) {
		
		Recipients findByEmail = recipientRepository.findByEmail(email);
		if(findByEmail==null) {
			return Collections.emptyList();
		}
		
		List<Recipients> listRecipient = Arrays.asList(findByEmail);
		
		return listRecipient.stream().map(this::toDto).collect(Collectors.toList());		
	}
	
	private RecipientDto toDto(Recipients entity) {

		if(entity == null) 
			return null;
		
		RecipientDto dto = new RecipientDto();

		dto.setId(entity.getId());
		dto.setFirstname(entity.getFirstname());
		dto.setLastname(entity.getLastname());
		dto.setCivility(entity.getCivility());
		dto.setAdress(entity.getAdress());
		dto.setCellPhone(entity.getCellPhone());
		dto.setCity(entity.getCity());
		dto.setEmail(entity.getEmail());
		dto.setZipCode(entity.getZipCode());
		dto.setLastUpdatePlace(entity.getLastUpdatePlace());
		dto.setLocalisation(entity.getLocalisation());
		dto.setLastUpdateDate(new Date().getTime());
		return dto;

	}
	private Recipients toEntity(RecipientDto dto) {
		

		if(dto == null) 
			return null;
		
		Recipients entity = new Recipients();
		
		entity.setId(dto.getId());
		entity.setFirstname(dto.getFirstname());
		entity.setLastname(dto.getLastname());
		entity.setCivility(dto.getCivility());
		entity.setAdress(dto.getAdress());
		entity.setCellPhone(dto.getCellPhone());
		entity.setCity(dto.getCity());
		entity.setZipCode(dto.getZipCode());
		entity.setEmail(dto.getEmail());
		entity.setLastUpdatePlace(dto.getLastUpdatePlace());
		entity.setLocalisation(dto.getLocalisation());
		entity.setLastUpdateDate(new Date().getTime());
		
		return entity;
		
	}

}
